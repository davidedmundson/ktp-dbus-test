/*
 *  Copyright (C) 2010 David Faure   <faure@kde.org>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License version 2 as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "kwhiteboard.h"
#include <KDE/KAction>
#include <KDE/KDebug>
#include <KDE/KLocale>
#include <KDE/KStatusBar>
#include <QtCore/QTimer>
#include <QtDBus/QDBusPendingReply>
#include "kwhiteboardwidget.h"
#include "PeerInterface.h"

KWhiteboard::KWhiteboard(const QDBusConnection& conn, QWidget *parent) :
    KXmlGuiWindow(parent),
    m_connection(conn)
{
    kDebug() << m_connection.name();
    setWindowIcon(KIcon(QLatin1String("applications-education")));
    setupGUI(Default, "kwhiteboardui.rc");

//     m_whiteboardWidget = new KWhiteboardWidget(this, m_connection);
//     setCentralWidget(m_whiteboardWidget);

    m_latencyLabel = new QLabel(this);
    m_latencyLabel->setContentsMargins(0, 0, 5, 0);
    m_latencyLabel->setFrameStyle(QFrame::Panel|QFrame::Sunken);
    statusBar()->addPermanentWidget(m_latencyLabel);
    startTimer(10000);
    m_timer.invalidate();
    m_latencyLabel->setText(i18n("Latency: ???"));
}



#include "kwhiteboard.moc"
