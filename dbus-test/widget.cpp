#include "widget.h"
#include "ui_widget.h"

#include "peer.h"
#include "PeerInterface.h"

#include <TelepathyQt/DBusTubeChannel>
#include <TelepathyQt/IncomingDBusTubeChannel>
#include <TelepathyQt/DBusTubeChannel>

#include <TelepathyQt/PendingDBusTubeConnection>
#include <TelepathyQt/Contact>

#include <KDebug>

class Participant : QObject
{
public:
    Participant(const QString &dbusService, const Tp::ContactPtr &contact);

Q_SIGNALS:
    void hostNameFound();
};

Widget::Widget(const Tp::DBusTubeChannelPtr &tubeChannel, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget),
    m_channel(tubeChannel),
    m_connection(QString())
{
    ui->setupUi(this);

    if (Tp::IncomingDBusTubeChannelPtr::qObjectCast(tubeChannel)) {
        Tp::IncomingDBusTubeChannelPtr incomingChannel = Tp::IncomingDBusTubeChannelPtr::qObjectCast(tubeChannel);
        connect(incomingChannel->acceptTube(), SIGNAL(finished(Tp::PendingOperation*)), SLOT(onAcceptTubeFinished(Tp::PendingOperation*)));
    } else {
        Tp::OutgoingDBusTubeChannelPtr outgoingChannel = Tp::OutgoingDBusTubeChannelPtr::qObjectCast(tubeChannel);
        connect(outgoingChannel->offerTube(QVariantMap()), SIGNAL(finished(Tp::PendingOperation*)), SLOT(onOfferTubeFinished(Tp::PendingOperation*)));
    }
    

    connect(tubeChannel.data(), SIGNAL(busNameAdded(QString,Tp::ContactPtr)), SLOT(onBusNamesChanged()));
    connect(tubeChannel.data(), SIGNAL(busNameRemoved(QString,Tp::ContactPtr)), SLOT(onBusNamesChanged()));
}

Widget::~Widget()
{
    delete ui;
}


void Widget::onBusNamesChanged()
{
    ui->listWidget->clear();
    if (!m_connection.isConnected()) {
        return;
    }

    qDebug() << "Getting stuff!";

    QHashIterator<QString, Tp::ContactPtr> it(m_channel->contactsForBusNames());
    while (it.hasNext()) {
        it.next();

        org::kde::DBus::Peer *peerIface = new org::kde::DBus::Peer(it.key(), "/peer", m_connection, this);
        QDBusPendingReply<QString>  pr = peerIface->GetMachineId();
        QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(pr, this);
        watcher->setProperty("contact", QVariant::fromValue(it.value()));
        connect(watcher, SIGNAL(finished(QDBusPendingCallWatcher*)), SLOT(onDBusCallFinished(QDBusPendingCallWatcher*)));

    }
    qDebug() << "DONE!";
}

void Widget::onDBusCallFinished(QDBusPendingCallWatcher *wc)
{
    QDBusPendingReply<QString> reply = *wc;
    qDebug() << "REPLY";
    Tp::ContactPtr contact= wc->property("contact").value<Tp::ContactPtr>();

    ui->listWidget->addItem(contact->id() + " = " +reply.value());
}

void Widget::onOfferTubeFinished(Tp::PendingOperation *op)
{
    Tp::PendingDBusTubeConnection *dbusop = qobject_cast<Tp::PendingDBusTubeConnection*>(op);
    if (!dbusop) {
        kWarning() << "This is not a PendingDBusTubeConnection";
    }

    m_connection = QDBusConnection::connectToPeer(dbusop->address(), dbusop->address().right(8));

    new Peer(m_connection);

    onBusNamesChanged();
}

void Widget::onAcceptTubeFinished(Tp::PendingOperation *op)
{
    onOfferTubeFinished(op);
}

