#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <TelepathyQt/DBusTubeChannel>
#include <TelepathyQt/PendingOperation>

#include <TelepathyQt/Contact>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(const Tp::DBusTubeChannelPtr &channel, QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui;
    Tp::DBusTubeChannelPtr m_channel;
    QDBusConnection m_connection;

private Q_SLOTS:
    void onBusNamesChanged();
    void onDBusCallFinished(QDBusPendingCallWatcher *wc);
    void onOfferTubeFinished(Tp::PendingOperation *op);
    void onAcceptTubeFinished(Tp::PendingOperation *op);
};

Q_DECLARE_METATYPE(Tp::ContactPtr)

#endif // WIDGET_H
